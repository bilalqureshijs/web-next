$(document).ready(function(){
	
	$('.tab-Clicked').on('click',function(){
		event.preventDefault();
		
		$('#tabs li').removeClass('active');
		$(this).parent().addClass('active');
		
		$('.tab-content .tab-pane').removeClass('active in');
		 var id = $(this).attr('href');
		 $(id).addClass('active in');
		 // $(".size-div").removeClass('active');
		 // $(".color-div").removeClass('active');
		 // $(".size-product .get-value").html('');
		 // $(".color-name span").html('');
	});


	$('.rating-click a').on('click touch', function(event){
		
		event.preventDefault();
		$('.review-clicked').toggleClass('not-collapsed');
		$('.review-clicked').toggleClass('collapsed');
		 var id = $('.review-clicked').attr('href');
		 $(id).slideToggle('slow',function () {
			  document.querySelector('#scroll-review-click').scrollIntoView({ 
					behavior: 'smooth' 
				});
		 });

		return false;
	});



	$(".size-div").on('click', function() {
		$('.messages-product p').removeClass('active');
		$('.in-stock').addClass('active');
	});
	$(".size-div-warning").on('click', function() {
		$('.messages-product p').removeClass('active');
		$('.in-warning').addClass('active');
	});
	$(".size-div-out-off-stock").on('click', function() {
		$('.messages-product p').removeClass('active');
		$('.out-of-stock').addClass('active');
	});

	$(".size-div-id").on('click', function() {
		$(".size-div-id").removeClass('active');
		$(this).addClass('active');
	});
	$(".color-div").on('click', function() {
		$(".color-div").removeClass('active');
		$(this).addClass('active');
	});

	$(".nav-tabs li").on('click', function() {
		var value = $(this).find('input').val();
		$('.fit span').html(value);
	});
	$(".color-div").on('click', function() {
		
		if ($(this).attr('value') == 'Green') {
			$('.color-name span').html($(this).attr('value'));
		} else if ($(this).attr('value') == 'Grey') {
			$('.color-name span').html($(this).attr('value'));
		} else if ($(this).attr('value') == 'Black') {
			$('.color-name span').html($(this).attr('value'));
		} 
		
	});
	$(".size-div-id").on('click', function() {
		$(".size-product .get-value").html($(this).attr('value'));
	});


	// popup section 
	$('.img-popup-click').on('click', function(){
		$('.img-popup-div').fadeIn();
	});

	$('.close-btn').on('click', function(){
		$('.img-popup-div').fadeOut();
	});

	$('#notify-me').on('click', function(){
		$('#notify-popup').animate({top: '34%'});
		$('.overlay').fadeIn();
	});

	$('.close-popup').on('click', function(){
		$('#notify-popup').animate({top: '-100%'})
		$('.overlay').fadeOut();
	});
	
	$('.email-cancel-image').on('click', function(){
		$('.notify-email').val("");
	});

	$('.check-validation').on('click', function(){
		var inputValue = $('.notify-email').val();
		var isValidEmail = isValidEmailAddress(inputValue);

		if (!isValidEmail) {
			$('.validation-msg').show();
			$('.email-cancel-image').show();
		} else {
			$('.validation-msg').hide();
			$('.email-cancel-image').hide();
			$('.notify-body').hide();
			$('.notify-heading').hide();
			$('.thankyou-body').show();
			$('.thankyou-heading').show();

		}

	});

	function isValidEmailAddress(emailAddress) {
	    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
	    return pattern.test(emailAddress);
	};


	// slider section
	$(".product-main").slick();
	$(".product-first").slick();
	$('.product-second').slick();


	// handle swiping options

	document.addEventListener('touchstart', handleTouchStart, false);
    document.addEventListener('touchmove', handleTouchMove, false);

    var xDown = null;
    var yDown = null;

    function handleTouchStart(evt) {
      xDown = evt.touches[0].clientX;
      yDown = evt.touches[0].clientY;
    };

    function handleTouchMove(evt) {
      if (!xDown || !yDown) {
        return;
      }
      
      
      var xUp = evt.touches[0].clientX;
      var yUp = evt.touches[0].clientY;

      var xDiff = xDown - xUp;
      var yDiff = yDown - yUp;
      var targetElement = evt.target || evt.srcElement;
      
      if (Math.abs(xDiff) > Math.abs(yDiff)) {/*most significant*/
        if (xDiff > 0) {
          // console.log();
          if($('#filter-wrapper').hasClass('slide-in')){
            if (!$(targetElement).parents('go-nicely-pic') || !$(targetElement).is('img')) {
              $('#filter-wrapper, .apply-filter-wrapper').removeClass('slide-in').addClass('sectionAnimation');
              $('.filter-arrow').fadeOut();
              toggleFilter();
              $('.overlay').fadeOut();
            } 
          } else if ($('#section-nav-wrapper').hasClass('slide-in')) {
              if (!$(targetElement).parents('go-nicely-pic') || !$(targetElement).is('img')) {
                $('.hamburger-slim').trigger('click');
              } 
            }

        } else {
          if($('#filter-wrapper').length == 1){
            if (!$(targetElement).parents('go-nicely-pic') || !$(targetElement).is('img')) {
              reuseableSwipeAndClickFilter();
            }  
          } else if ($('#section-nav-wrapper').length == 1 ) {
            if (!$(targetElement).parents('go-nicely-pic') || !$(targetElement).is('img')) {
              $('.hamburger-slim').trigger('click');
            } 
          } 
          /* right swipe */
        }
      } else {
        if (yDiff > 0) {
          /* up swipe */
        } else {
          /* down swipe */
        }
      }
      /* reset values */
      xDown = null;
      yDown = null;
    };
});
$(document).on('click', '[data-toggle="lightbox"]', function(event) {
	event.preventDefault();
	$(this).ekkoLightbox();
});
		