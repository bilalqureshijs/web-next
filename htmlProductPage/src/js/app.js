
  $(document).ready(function() {

    //var headerBg = sessionStorage.getItem('headerBg');
    //$('.header').addClass(headerBg);
    setHeaderBg();

    


    // language toggle active 
    $(".language-box").on('click', function() {
        $(".language-box").removeClass('active');
        $(this).addClass('active');
      });


    // clear search box func
    $('.clear').on('click', function(){
      $('#search-input').val('')
    });

    // clear search box filter section
    $('.clear2').on('click', function(){
      $('#search-input2').val('')
    });

    //keeping this just for next version
    //$('.overlay, .search-overlay, .invisible-overlay, .header-wrapper, .header').on('click', function(){});


    // body click or touch event 
    // checking here where clicked
    // and than deciding the action through if statments
    $('body').on('click touch', function(e) {
      // e.stopPropagation()

      // checking target element
      var target = $(e.target), article;
      // alert(target.is);

      
      if (target.is('div.overlay')) {
        // if clicked  closing everything back grey overlay 
        if ($('#section-nav-wrapper').hasClass('slide-in')) {
          $('#section-nav-wrapper').removeClass('slide-in');
          $('.overlay').fadeOut();
          $('.hamburger-slim').removeClass('active');
          isBurgerActive();
        }
        
        if ($('#shopping-bag').hasClass('slide-in')) {
          $('#shopping-bag, .shopping-bag-top').removeClass('slide-in');
          $('#shopping-bag').one(transEndEventName, function(){
            $('#shopping-bag').css({'z-index': -10});
          });

          $('html, body').removeClass('overflow-hidden');
          $('.overlay').fadeOut();
          $('.basket-arrow').fadeOut();
        }

        if ($('#filter-wrapper').hasClass('slide-in')) {
          $('#filter-wrapper, .apply-filter-wrapper').removeClass('slide-in');
          toggleFilter();
          $('.filter-arrow').fadeOut();
          $('.overlay').fadeOut();
        }
      }

      // recent search event removing
      if($('#recent-search').hasClass('active')){
        if(target.is('.search-overlay') || target.is('.header-wrapper') || target.is('.header')){
          removeSuggestedSearch();
        }
      }
      // suggested search event removing
      if($('#suggested-search').hasClass('active')){
        if(target.is('.search-overlay') || target.is('.header-wrapper') || target.is('.header')){
          removeRecentSearch();
        }
      }

      // sorting search event removing
      if($('#sort-wrapper').hasClass('slide-in')){
        if (target.is('.invisible-overlay') || target.is('.header-wrapper') || target.is('.header')) {
          $('.sort-arrow').fadeOut();
          animateSort();
        }
      }

      if($('#more-wrapper').hasClass('slide-in')){
        if (target.is('.invisible-overlay') || target.is('.header-wrapper') || target.is('.header')) {
          $('.more-arrow').fadeOut();
          animateMore();
        }
      }
    });

    $('#search-product-item').css({'border': '1px solid red;'});


    $('#more-wrapper ul li').on('click', function() {

      $('#more-wrapper ul li span').removeClass('active');
      if (!$(this).children('span').hasClass('active')) {
        $(this).children('span').toggleClass('active');
      }
      // events for product listing page 
      // stacking event to change the layouts
      // and images size

      // 17-04 change
      if ($(this).data('set') == 'one') {
        // 24-04 change
        $('.search-product #search-product-item').removeClass('col-xs-6 col-sm-6 col-md-6 col-lg-6 ');
        $('.search-product #search-product-item').removeClass('col-xs-12 col-sm-12 col-md-12 col-lg-12');

        $('.search-product #search-product-item').addClass('col-xs-12 col-sm-4 col-md-4 col-lg-4');
        //$('.search-product #search-product-item .info-written img').css({'display': 'none'});
        $('.search-product #search-product-item .info-written').css({'padding-top': '15px'});

        $('.search-product #search-product-item .info-image').removeClass('col-xs-6');
        $('.search-product #search-product-item .info-image').addClass('col-xs-12');

        $('.search-product #search-product-item .info-written').removeClass('col-xs-6');
        $('.search-product #search-product-item .info-written').addClass('col-xs-12');
        $('.search-images-sec').css('padding','0');
        
        $('.custom-padding-left-img').addClass('padding-upper-than-992');
        $('.custom-padding-right-img').addClass('padding-upper-than-992');

        $('.custom-padding-left-img').css('padding','0');
        $('.custom-padding-right-img').css('padding','0');
        

      } else if ($(this).data('set') == 'two') {
        // 24-04 change
        $('.search-product #search-product-item').removeClass('col-xs-12 col-sm-12 col-md-12 col-lg-12');
        $('.search-product #search-product-item').removeClass('col-xs-12 col-sm-4 col-md-4 col-lg-4');
        
        $('.search-product #search-product-item').addClass('col-xs-6 col-sm-6 col-md-6 col-lg-6');
        //$('.search-product #search-product-item .info-written img').css({'display': 'none'});
        $('.search-product #search-product-item .info-written').css({'padding-top': '15px'});

        $('.search-product #search-product-item .info-image').removeClass('col-xs-6');
        $('.search-product #search-product-item .info-image').addClass('col-xs-12');

        $('.search-product #search-product-item .info-written').removeClass('col-xs-6');
        $('.search-product #search-product-item .info-written').addClass('col-xs-12');
        $('.search-images-sec').css('padding','0 20px');
        // 17-04 change
         $('.custom-padding-left-img').css('padding','0 15px 0 15px');
        $('.custom-padding-right-img').css('padding','0 15px 0 15px');

      } else if ($(this).data('set') == 'info') {
        // 24-04 change
        $('.search-product #search-product-item').removeClass('col-xs-6 col-sm-6 col-md-6 col-lg-6');
        $('.search-product #search-product-item').removeClass('col-xs-12 col-sm-4 col-md-4 col-lg-4');

        $('.search-product #search-product-item').addClass('col-xs-12 col-sm-12 col-md-12 col-lg-12');

        $('.search-product #search-product-item .info-image').removeClass('col-xs-12');
        $('.search-product #search-product-item .info-image').addClass('col-xs-6');

        $('.search-product #search-product-item .info-written').removeClass('col-xs-12');
        $('.search-product #search-product-item .info-written').addClass('col-xs-6');

        $('.search-product #search-product-item .info-written').css({'padding-top': '24%'});// 17-04 change
        //$('.search-product #search-product-item .info-written img').css({'display': 'block'});
        $('.search-images-sec').css('padding','0 20px');
        // 17-04 change
         $('.custom-padding-left-img').css('padding','0 15px 0 15px');
        $('.custom-padding-right-img').css('padding','0 15px 0 15px');
    }
    });


// search events start

    var loginFormHeight = $('#login').height(); //get login wrapper height
    var recentSearchHeight = $('#recent-search').height(); //get recent search wrapper height
    var suggestedSearchHeight = $('#suggested-search').height(); //get suggested search wrapper height



    // stacking up events to show search
    $('#filter-header').css({top: $('header').outerHeight(true)});
    $('#filter-wrapper').css({top: $('#filter-header').outerHeight(true) + $('#header').outerHeight(true) + 'px'});
    //$('#sort-wrapper').css({top: $('#filter-header').outerHeight(true) + $('#header').outerHeight(true) + 'px'});
    //$('#more-wrapper').css({top: $('#filter-header').outerHeight(true) + $('#header').outerHeight(true) + 'px'});
    $('#added-item').css({top: $('#header').outerHeight(true) + 'px'});
    $('#login').css({top: '-' + loginFormHeight + 'px'});
    $('#recent-search').css({top: '-' + recentSearchHeight + 'px'});
    $('#suggested-search').css({top: '-' + 10 + suggestedSearchHeight + 'px'});

    $("body").css("visibility", "visible");

    //search field auto focus
    $('#search-wrapper').on('focus', "input[type=text]", function() {
      $('#recent-search').animate({top: $('#header').outerHeight(true) + 45 + 'px'},{ duration: 400, queue: false }).addClass('active');
      $('.search-overlay').fadeIn();
    });

    // suggested field fadeIn effect
    $("#search-wrapper input[type=text]").keydown(function() {
      $('#suggested-search').css({top: $('#header').outerHeight(true) + 45 + 'px'}).addClass('active');
      $('.search-overlay').fadeIn();
    });


    // click on flag
    $('#select-country').click(function () {
      $('#child-select-lang-country').slideToggle();
      $('.arrow-english-right').toggleClass('remove-transform');
    });

    // click on Shop by range
    $('.shop-by-range').click(function () {
      $('.shop-by-range-sec').slideToggle();
      $('.arrow-english-right2').toggleClass('remove-transform');
    });


    // swtching header 
    $('.header-switch').on('click', function(){
      $('.header').toggleClass('black');
      if($('.header').hasClass('black')){
        sessionStorage.setItem('headerBg', 'black');
      } else {
        sessionStorage.clear();
      }
      setHeaderBg();
    });
    // header switching event binding method
    function setHeaderBg(){
      if($('.header').hasClass('black')){

        $('img.hamburger-slim').attr("src", "src/images/Menu-white.svg");
        $('img.search').attr("src", "src/images/icn-search-white.svg");
        $('img.bag').attr("src", "src/images/icn-bag-white.svg");
        $('img.profile').attr("src", "src/images/icn-profile-white.svg");
        $('img.logo').attr("src", "src/images/logo-next-white.svg");

        if($('#section-nav-wrapper').hasClass('slide-in')){
          $('.hamburger-slim').attr("src", "src/images/Close-white.svg");
        }

      } else {

        $('img.hamburger-slim').attr("src", "src/images/Menu.svg");
        $('img.search').attr("src", "src/images/icn-search.svg");
        $('img.bag').attr("src", "src/images/icn-bag.svg");
        $('img.profile').attr("src", "src/images/icn-profile.svg");
        $('img.logo').attr("src", "src/images/logo-next.svg");

        if($('#section-nav-wrapper').hasClass('slide-in')){
          $('.hamburger-slim').attr("src", "src/images/Close.svg");
        }

      }
    }

    // clicked on apply filter and then toggling it.
    $('#apply-filter').on('click', function(){
      animateFilter();
      toggleFilter();
      $('.overlay').fadeToggle();
    });

    // swipe functionality added
    function reuseableSwipeAndClickFilter(){
      if ($('#sort-wrapper').hasClass('slide-in')) {
        $('#sort-wrapper').toggleClass('slide-in');
        $('.sort-arrow').fadeOut();
        $('#sort .text').text('SORT');//  07-04 change
        $('#sort-wrapper').one(transEndEventName,
            function(e) {
              animateFilter();
              toggleFilter();
              $('.overlay').fadeToggle();
            });
      } else if ($('#more-wrapper').hasClass('slide-in')) {
        $('#more-wrapper').toggleClass('slide-in');
        $('.more-arrow').fadeOut();
        $('#more .text').text('MORE');//  07-04 change
        $('#more-wrapper').one(transEndEventName,
            function(e) {
              animateFilter();
              toggleFilter();
              $('.overlay').fadeToggle();
            });
      } else if ($('#shopping-bag').hasClass('slide-in')) {
        $('.basket-arrow').fadeOut();
        $('#shopping-bag, .shopping-bag-top').toggleClass('slide-in');
        $('#shopping-bag').one(transEndEventName,
            function(e) {
              animateFilter();
              toggleFilter();
            });
      } else if ($('#section-nav-wrapper').hasClass('slide-in')) {
        $('#section-nav-wrapper').toggleClass('slide-in');
        $('#section-nav-wrapper').one(transEndEventName,
            function(e) {
              animateFilter();
              toggleFilter();
              $('.overlay').fadeToggle();
            });
      } else if (!$('#search-wrapper').hasClass('not-active')) {
        $('.search-arrow').fadeOut();
        $('#search-wrapper').animate({top: 0}, function() {
          animateFilter();
          toggleFilter();
          $('.overlay').fadeToggle();
        });
        $('#search-wrapper').toggleClass('not-active');

      }
      else {
        animateFilter();
        toggleFilter();
        $('.overlay').fadeToggle();
      }
    }

    // sorting function for sort more 
    function backgroundTouchForSortMore(){
      if($('#sort-wrapper').hasClass('slide-in')) {
        $('.invisible-overlay').fadeIn();
      } else if($('#more-wrapper').hasClass('slide-in')) {
        $('.invisible-overlay').fadeIn();
      } else {
        $('.invisible-overlay').fadeOut();
      }
    }

    // toggle filter events 
    function toggleFilter() {
       $('#filter .text').text('FILTER');
      if ($('#filter-wrapper').hasClass('slide-in')) {
        $('#filter .text').text('CLOSE');
        $('html, body').addClass('overflow-hidden');
      } else {
        $('#filter .text').text('FILTER');
        $('html, body').removeClass('overflow-hidden');
      }
    }

    // checking if the burger is active or not
    function isBurgerActive() {
      if($('.header').hasClass('black')){
        if ($('.hamburger-slim').hasClass('active')) {
          $('.sidebar-arrow').fadeIn();
          $('.hamburger-slim').attr("src", "src/images/Close-white.svg");
          $('html, body').addClass('overflow-hidden');
        } else {
          $('.sidebar-arrow').fadeOut();
          $('.hamburger-slim').attr("src", "src/images/Menu-white.svg");
          $('html, body').removeClass('overflow-hidden');
        }
      } else {
        if ($('.hamburger-slim').hasClass('active')) {
          $('.sidebar-arrow').fadeIn();
          $('.hamburger-slim').attr("src", "src/images/Close.svg");
          $('html, body').addClass('overflow-hidden');
        } else {
          $('.sidebar-arrow').fadeOut();
          $('.hamburger-slim').attr("src", "src/images/Menu.svg");
          $('html, body').removeClass('overflow-hidden');
        }
      }
    }

    //removing suggesting search and recent search 
    function removeSuggestedSearch(){
      $('#recent-search').animate({top: '-' + recentSearchHeight + 'px'},{ duration: 400, queue: false }).removeClass('active');
      $('.search-overlay').fadeOut();
    }
    function removeRecentSearch(){
      $('#suggested-search').css({top: '-' + 10 + suggestedSearchHeight + 'px'}).removeClass('active');
      $('.search-overlay').fadeOut();
    }

    // shopping bag animation
    function animateShoppingBag() {
      $('#shopping-bag').css({top: $('#header').outerHeight(true) + 'px'}).toggleClass('slide-in').addClass('sectionAnimation3');
      $('.shopping-bag-top').toggleClass('slide-in').addClass('sectionAnimation3');

      if ($('#shopping-bag').hasClass('slide-in')) {
        $('.basket-arrow').fadeIn();
        $('html, body').addClass('overflow-hidden');
        $('#shopping-bag').css({'z-index': 999});
        $('.overlay').fadeIn();
      } else {
        $('.basket-arrow').fadeOut();
        $('html, body').removeClass('overflow-hidden');
        $('.overlay').fadeOut();
        $('#shopping-bag').one(transEndEventName,
            function(e) {
              $('#shopping-bag').css({'z-index': -10});
          });
      }
    }

    // keyboard opeing and fucusing 
    (function openKeyboard(){
      document.getElementById("search").addEventListener('click', function(){
        document.getElementById("search-input").focus();
      });

      document.getElementById("search").addEventListener('tap', function(){
        document.getElementById("search-input").focus();
      });

      document.getElementById("search").addEventListener('touch', function(){
        document.getElementById("search-input").focus();
      });
    })();

    function animateFilter() {

      $('#filter-wrapper').toggleClass('slide-in').addClass('sectionAnimation');
      $('.apply-filter-wrapper').toggleClass('slide-in').addClass('sectionAnimation');
      $('#filter-header ul li').removeClass('active');//  07-04 change
      if($('#filter-wrapper').hasClass('slide-in')){
        $('.filter-arrow').fadeIn();
        $('#filter').addClass('active');//  07-04 change
      } else {
        $('.filter-arrow').fadeOut();
      }
       toggleFilter();
      backgroundTouchForSortMore();

    }
    function animateSort() {
      $('#sort-wrapper').toggleClass('slide-in').addClass('sectionAnimation4');
      $('#filter-header ul li').removeClass('active');//  07-04 change
      if($('#sort-wrapper').hasClass('slide-in')){
        $('.sort-arrow').fadeIn();
        $('#sort').addClass('active');//  07-04 change
      } else {
        $('.sort-arrow').fadeOut();
        
      }

      backgroundTouchForSortMore();
      toggleSort();//  07-04 change
    }
    // 07-04 change
    function toggleSort() {
      $('#sort .text').text('SORT');
      if ($('#sort-wrapper').hasClass('slide-in')) {
        $('#sort .text').text('CLOSE');
        $('html, body').addClass('overflow-hidden');
      } else {
        $('#sort .text').text('SORT');
        $('html, body').removeClass('overflow-hidden');
      }
    }
    function animateMore(){
      //  07-04 change
      $('#more-wrapper').toggleClass('slide-in').addClass('sectionAnimation4'); 
      $('#filter-header ul li').removeClass('active');//  07-04 change
      if($('#more-wrapper').hasClass('slide-in')){
        $('.more-arrow').fadeIn();
        $('#more').addClass('active');//  07-04 change
      } else {
        $('.more-arrow').fadeOut(); 
      }
      backgroundTouchForSortMore();
      toggleMore();
    }
    //  07-04 change
    function toggleMore() {
      $('#more .text').text('MORE');
      if ($('#more-wrapper').hasClass('slide-in')) {
        $('#more .text').text('CLOSE'); 
        $('html, body').addClass('overflow-hidden');
      } else {
        $('#more .text').text('MORE');
        $('html, body').removeClass('overflow-hidden');
      }
    }
    function animateSideBar(){
      $('#section-nav-wrapper').css({top: $('#header').outerHeight(true) + 'px'}).toggleClass('slide-in').addClass('sectionAnimation');
      $('.hamburger-slim').toggleClass('active');
      isBurgerActive();
    }
    function animateSearch() {
      
      if ($('#search-wrapper').hasClass('not-active')) {
        //$('.overlay').fadeIn();
        $('.search-arrow').fadeIn();
        $('#search-wrapper').animate({top: $('#header').outerHeight(true) + 'px'}, {duration: 400, queue: false}, function(){
          $( ".search" ).focus();
          // openKeyboard();
        });

      } else {
        $('.search-arrow').fadeOut();
        //$('.overlay').fadeOut();
        removeSuggestedSearch();
        removeRecentSearch();
        $('#search-wrapper').animate({top: 0});
        $( ".search" ).blur();
      }
      $('#search-wrapper').toggleClass('not-active');
    }
    function animateLogin(){
      if ($('#login').hasClass('not-active')) {
        $('.profile-arrow').fadeIn();
        $('#login').animate({top: 0 + $('#header').outerHeight(true) + 'px'});
        $('#login').removeClass('not-active');
      } else {
        $('.profile-arrow').fadeOut();
        $('#login').animate({top: '-' + loginFormHeight + 'px'})
        $('#login').addClass('not-active');
      }
    }


    // clicking on search button 
    // closing other animateable items and opening search
    $('#search').on('click', function() {
      
      if ($('#section-nav-wrapper').hasClass('slide-in')) {
        $('#section-nav-wrapper').toggleClass('slide-in');
        $('.overlay').fadeToggle();
        $('.hamburger-slim').toggleClass('active');
        isBurgerActive();
        animateSearch();
        // $('#section-nav-wrapper').one(transEndEventName,
        //     function(e) {
              
        //     });
      } else if ($('#shopping-bag').hasClass('slide-in')) {
        $('.basket-arrow').fadeOut();
        $('.overlay').fadeToggle();
        $('#shopping-bag, .shopping-bag-top').toggleClass('slide-in');
        animateSearch();
        // $('#shopping-bag').one(transEndEventName,
        //     function(e) {
              
        //     });
      } else if ($('#sort-wrapper').hasClass('slide-in')) {
        // $('#sort-wrapper').toggleClass('slide-in');
        // $('.sort-arrow').fadeOut();
        // 07-04 change
        animateSort();
        animateSearch();
        // $('#sort-wrapper').one(transEndEventName,
        //     function(e) {
              
        //     });
      } else if ($('#more-wrapper').hasClass('slide-in')) {
        // $('#more-wrapper').toggleClass('slide-in');
        // $('.more-arrow').fadeOut();
        // 07-04 change
        animateMore();
        animateSearch();
        // $('#more-wrapper').one(transEndEventName,
        //     function(e) {
              
        //     });
      } else if ($('#filter-wrapper').hasClass('slide-in')) {
        // 07-04 change
         $('.overlay').fadeToggle();
        // $('#filter-wrapper, .apply-filter-wrapper').toggleClass('slide-in');
        // $('.filter-arrow').fadeOut();
        animateFilter();
        animateSearch();
        // $('#filter-wrapper').one(transEndEventName,
        //     function(e) {
              
        //     });
      } else if (!$('#login').hasClass('not-active')) {
        $('.profile-arrow').fadeOut();
        $('#login').toggleClass('not-active');
        animateSearch();
        $('#login').animate({top: '-' + loginFormHeight + 'px'})
      } else {
        animateSearch();
      }
    });


    // opening profile
    // closing other items
    $('.user-profile').on('click', function() {
      if ($('#section-nav-wrapper').hasClass('slide-in')) {
        $('#section-nav-wrapper').toggleClass('slide-in');
        $('#section-nav-wrapper').one(transEndEventName,
            function(e) {
              $('.overlay').fadeToggle();
              $('.hamburger-slim').toggleClass('active');
              isBurgerActive();
              animateLogin();
            });
      } else if ($('#shopping-bag').hasClass('slide-in')) {
        $('.basket-arrow').fadeOut();
        $('#shopping-bag, .shopping-bag-top').toggleClass('slide-in');
        $('#shopping-bag').one(transEndEventName,
            function(e) {
              $('.overlay').fadeToggle();
              animateLogin();
            });
      } else if ($('#sort-wrapper').hasClass('slide-in')) {
        $('#sort-wrapper').toggleClass('slide-in');
        $('.sort-arrow').fadeOut();
        // 07-04 change
        toggleSort();
        $('#sort-wrapper').one(transEndEventName,
            function(e) {
              animateLogin();
            });
      } else if ($('#more-wrapper').hasClass('slide-in')) {
        $('#more-wrapper').toggleClass('slide-in');
        $('.more-arrow').fadeOut();
        // 07-04 change
        toggleMore();
        $('#more-wrapper').one(transEndEventName,
            function(e) {
              animateLogin();
            });
      } else if ($('#filter-wrapper').hasClass('slide-in')) {
        $('#filter-wrapper, .apply-filter-wrapper').toggleClass('slide-in');
        $('.filter-arrow').fadeOut();
        $('#filter-wrapper').one(transEndEventName,
            function(e) {
              toggleFilter();
              $('.overlay').fadeToggle();
              animateLogin();
            });
      } else if (!$('#search-wrapper').hasClass('not-active')) {
        removeSuggestedSearch();
        removeRecentSearch();
        $('.search-arrow').fadeOut();
        $('#search-wrapper').animate({top: 0}, function() {
          animateLogin();
        });

        $('#search-wrapper').toggleClass('not-active');

      } else {
        animateLogin();
      }
    });

    // changin icon
    // for add to bad and then setting it back after 3 sec
    $('#add-to-bag').on('click', function() {
      var count = parseInt($('.basket-count').text());
      $('.basket-count').text();
      $('#added-item').fadeIn('slow', function() {
        $(this).delay(3000).fadeOut('slow');
      });
      count = count + 1;
      $('.basket-count').text(count);
      var elem = $(this);
      elem.val('');
      elem.css('background','url(src/images/checked.png) no-repeat center center #000');
      setTimeout(function(){
        elem.css('background-image','none');
        elem.val('ADD TO BAG');
      }, 3000);
    });


    // cart clicked and animating other out
    $('.cart').on('click', function() {
      if ($('#sort-wrapper').hasClass('slide-in')) {
        $('#sort-wrapper').toggleClass('slide-in');
        $('.sort-arrow').fadeOut();
        // 07-04 change
        toggleSort();
        $('#sort-wrapper').one(transEndEventName,
            function(e) {
              $('.overlay').fadeToggle();
              animateShoppingBag();
            });
      } else if ($('#more-wrapper').hasClass('slide-in')) {
        $('#more-wrapper').toggleClass('slide-in');
        $('.more-arrow').fadeOut();
        // 07-04 change
        toggleMore();
        $('#more-wrapper').one(transEndEventName,
            function(e) {
              $('.overlay').fadeToggle();
              animateShoppingBag();
            });
      } else if ($('#filter-wrapper').hasClass('slide-in')) {
        $('#filter-wrapper, .apply-filter-wrapper').toggleClass('slide-in');
        $('.filter-arrow').fadeOut();
        $('#filter-wrapper').one(transEndEventName,
            function(e) {
              toggleFilter();
              animateShoppingBag();
            });
      } else if ($('#section-nav-wrapper').hasClass('slide-in')) {
        $('#section-nav-wrapper').toggleClass('slide-in');
        $('#section-nav-wrapper').one(transEndEventName,
            function(e) {
              $('.hamburger-slim').toggleClass('active');
              isBurgerActive();
              animateShoppingBag();
            });
      } else if (!$('#login').hasClass('not-active')) {
        $('.profile-arrow').fadeOut();
        $('#login').animate({top: '-' + loginFormHeight + 'px'}, function() {
          $('#login').toggleClass('not-active');
          $('.overlay').fadeToggle();
          animateShoppingBag();
        })
      } else if (!$('#search-wrapper').hasClass('not-active')) {
        $('.overlay').fadeToggle();
        removeSuggestedSearch();
        removeRecentSearch();
        $('.search-arrow').fadeOut();
        $('#search-wrapper').animate({top: 0}, function() {
          animateShoppingBag();
        });
        $('#search-wrapper').toggleClass('not-active');

      } else {
        $('.overlay').fadeToggle();
        animateShoppingBag();
      }
    });


    $('#filter').on('click', function() {
      reuseableSwipeAndClickFilter();
    });

    $('#sort').on('click', function() {
      if ($('#filter-wrapper').hasClass('slide-in')) {
        $('#filter-wrapper, .apply-filter-wrapper').toggleClass('slide-in');
        $('.filter-arrow').fadeOut();
        $('#filter .text').text('FILTER');//  07-04 change
        $('#filter-wrapper').one(transEndEventName,
            function(e) {
              toggleFilter();
              animateSort();
              $('.overlay').fadeToggle();
            });
      } else if ($('#more-wrapper').hasClass('slide-in')) {
        $('#more-wrapper').toggleClass('slide-in');
        $('.more-arrow').fadeOut();
        $('#more .text').text('MORE');//  07-04 change
        $('#more-wrapper').one(transEndEventName,
            function(e) {
              animateSort();
            });
      } else if ($('#shopping-bag').hasClass('slide-in')) {
        $('.basket-arrow').fadeOut();
        $('#shopping-bag, .shopping-bag-top').toggleClass('slide-in');
        $('#shopping-bag').one(transEndEventName,
            function(e) {
              animateSort();
            });
      } else if ($('#section-nav-wrapper').hasClass('slide-in')) {
        $('#section-nav-wrapper').toggleClass('slide-in');
        $('#section-nav-wrapper').one(transEndEventName,
            function(e) {
              animateSort();
            });
      } else if (!$('#search-wrapper').hasClass('not-active')) {
        $('.search-arrow').fadeOut();
        $('#search-wrapper').animate({top: 0}, function() {
          animateSort();
        });
        $('#search-wrapper').toggleClass('not-active');
      } else {
        animateSort();
      }
    });

    $('#more').on('click', function() {

      if ($('#filter-wrapper').hasClass('slide-in')) {
        $('#filter-wrapper, .apply-filter-wrapper').toggleClass('slide-in');
        $('.filter-arrow').fadeOut();
        $('#filter .text').text('FILTER');//  07-04 change
        $('#filter-wrapper').one(transEndEventName,
            function(e) {
              toggleFilter();
              $('.overlay').fadeToggle();
              animateMore();
            });
      } else if ($('#sort-wrapper').hasClass('slide-in')) {
        $('#sort-wrapper').toggleClass('slide-in');
        $('.sort-arrow').fadeOut();
        $('#sort .text').text('SORT');//  07-04 change
        $('#sort-wrapper').one(transEndEventName,
            function(e) {
              animateMore();
            });
      } else if ($('#shopping-bag').hasClass('slide-in')) {
        $('.basket-arrow').fadeOut();
        $('#shopping-bag, .shopping-bag-top').toggleClass('slide-in');
        $('#shopping-bag').one(transEndEventName,
            function(e) {
              animateMore();
            });
      } else if ($('#section-nav-wrapper').hasClass('slide-in')) {
        $('#section-nav-wrapper').toggleClass('slide-in');
        $('#section-nav-wrapper').one(transEndEventName,
            function(e) {
              $('.overlay').fadeToggle();
              $('.hamburger-slim').toggleClass('active');
              isBurgerActive();
              animateMore();
            });
      } else if (!$('#search-wrapper').hasClass('not-active')) {
        $('.search-arrow').fadeOut();
        $('#search-wrapper').animate({top: 0}, function() {
          animateMore();
        });

        $('#search-wrapper').toggleClass('not-active');

      } else {
        animateMore();
      }
    });

    $('.hamburger-slim').click(function() {
      
      if ($('#filter-wrapper').hasClass('slide-in')) {
        $('#filter-wrapper, .apply-filter-wrapper').toggleClass('slide-in');
        $('.filter-arrow').fadeOut();
        $('#filter-wrapper').one(transEndEventName,
            function(e) {
              toggleFilter();
              animateSideBar();
            });
      } else if ($('#more-wrapper').hasClass('slide-in')) {
        $('#more-wrapper').toggleClass('slide-in');
        $('.more-arrow').fadeOut();
        // 07-04 change
        toggleMore();
        $('#more-wrapper').one(transEndEventName,
            function(e) {
              animateSideBar();
              $('.overlay').fadeToggle();
            });
      } else if ($('#sort-wrapper').hasClass('slide-in')) {
        $('#sort-wrapper').toggleClass('slide-in');
        $('.sort-arrow').fadeOut();
        // 07-04 change
        toggleSort();
        $('#sort-wrapper').one(transEndEventName,
            function(e) {
              animateSideBar();
              $('.overlay').fadeToggle();
            });
      } else if ($('#shopping-bag').hasClass('slide-in')) {
        $('.basket-arrow').fadeOut();
        $('#shopping-bag, .shopping-bag-top').toggleClass('slide-in');
        $('#shopping-bag').one(transEndEventName,
            function(e) {
              animateSideBar();
            });
      } else if (!$('#login').hasClass('not-active')) {
        $('.profile-arrow').fadeOut();
        $('#login').animate({top: '-' + loginFormHeight + 'px'}, function() {
          $('#login').toggleClass('not-active');
          animateSideBar();
          $('.overlay').fadeToggle();
        })
      } else if (!$('#search-wrapper').hasClass('not-active')) {
        removeSuggestedSearch();
        removeRecentSearch();
        $('.search-arrow').fadeOut();
        $('#search-wrapper').animate({top: 0}, function() {
          animateSideBar();
          //$('.overlay').fadeToggle();
        });
        $('#search-wrapper').toggleClass('not-active');

      } else {
        animateSideBar();
        $('.overlay').fadeToggle();
      }
    });

    $('#filter-color').on('click', function(){
      $('.color-filter-wrapper').slideToggle().toggleClass('closed');
      if($('.color-filter-wrapper').hasClass('closed')){
        $('#filter-color img').attr("src", "src/images/Plus2.svg");
      } else {
        $('#filter-color img').attr("src", "src/images/Minus.svg");
      }
    });

    // 07-04 change
    $('#filter-price').on('click', function(){
      $('.price-filter-wrapper').slideToggle().toggleClass('closed');
      if($('.price-filter-wrapper').hasClass('closed')){
        $('#filter-price img').attr("src", "src/images/Plus2.svg");
      } else {
        $('#filter-price img').attr("src", "src/images/Minus.svg");
      }
    });

    $('#filter-size').on('click', function(){
      $('.size-filter-wrapper').slideToggle().toggleClass('closed');
      if($('.size-filter-wrapper').hasClass('closed')){
        $('#filter-size img').attr("src", "src/images/Plus2.svg");
      } else {
        $('#filter-size img').attr("src", "src/images/Minus.svg");
      }
    });

    $('#filter-brand').on('click', function(){
      $('.brand-filter-wrapper').slideToggle().toggleClass('closed');
      if($('.brand-filter-wrapper').hasClass('closed')){
        $('#filter-brand img').attr("src", "src/images/Plus2.svg");
      } else {
        $('#filter-brand img').attr("src", "src/images/Minus.svg");
      }
    });

    // 07-04 change
    $('#view-more').on('click', function(){
      
      $('.view-more-container').slideToggle().toggleClass('closed');
      if($('.view-more-container').hasClass('closed')){
        $(this).text('- VIEW LESS');
      } else {
        $(this).text('+ VIEW MORE');
      }
    });

  });

  var windowScrollTop = '';

  // element to detect scroll direction of
  var el = $(window),

      // initialize last scroll position
      lastY = el.scrollTop(),
      lastX = el.scrollLeft();

  el.on('scroll', function() {
    // get current scroll position
    var currY = el.scrollTop(),
        currX = el.scrollLeft(),
        // determine current scroll direction
        // x = (currX > lastX) ? 'right' : ((currX === lastX) ? 'none' : 'left'),
        y = (currY > lastY) ? 'down' : ((currY === lastY) ? 'none' : 'up');
    if (y == 'up') {
      // $('.search-wrapper').addClass('slide-down');
    } else {
      // $('.search-wrapper').removeClass('slide-down');
    }
    // do something here…

    // update last scroll position to current position
    lastY = currY;
    lastX = currX;
  });


  $(window).scroll(function() {

    if (!$('main').hasClass('is-hidden')) {
      windowScrollTop = $(window).scrollTop();
    }

  });


  $('.child-general-ul').css({top: $('nav').outerHeight(true)});

  var state = [];

  $('li.title').on('click', function() {
    $(this).addClass('active');

    bottom = $('.nav').position().top + $('.nav').outerHeight(true);

    var child = '#child-' + $(this).siblings().data('child');
    var active = '#' + $(this).parent().attr('id');

    _.each(_.reverse(state.slice()), function(eL) {
      if (eL === active) {
        return false;
      }

      $(eL).animate({top: bottom}, 500, function() {
        $(eL).css({'display': 'none', 'z-index': 1});
        $(eL).removeClass('nav-active')
      });
    });

    var newState = [];
    _.each(state, function(el) {
      newState.push(el);
      if (el === active) {
        return false;
      }
    });

    state = newState;

  });

  $('.parent').on('click', function() {

    var nav = $('li.active').outerHeight();
    $('#all-departments-title').removeClass('active');
    $(this).siblings('.title').removeClass('active');

    var child = '#child-' + $(this).data('child');

    if ($(this).parent().attr('id') == 'all-department') {
      nav = 1
    }

    state.push(child);

    $(child).children('.title').addClass('active');
    $(child).addClass('nav-active');
    $(child).css({
      'z-index': 1000 + parseInt($('.nav-active').length, 10),
      'display': 'block'
    }).animate({top: nav}, 500);
  });

  $('#all-departments-title').on('click', function() {
    state = [];
    $('#all-departments-title').addClass('active');
    bottom = $('.nav').position().top + $('.nav').outerHeight(true);
    $('.nav-active').animate({top: bottom}, 500, function() {
      $('.nav-active').css({'display': 'none'});
    });
  });


// transition setting
  function whichTransitionEvent() {
    var el = document.createElement('fake'),
        transEndEventNames = {
          'WebkitAnimation': 'webkitAnimationEnd',// Saf 6, Android Browser
          'MozAnimation': 'animationend',      // only for FF < 15
          'animation': 'animationend'       // IE10, Opera, Chrome, FF 15+, Saf 7+
        };

    for (var t in transEndEventNames) {
      if (el.style[t] !== undefined) {
        return transEndEventNames[t];
      }
    }
  }

  var transEndEventName = whichTransitionEvent();

